#!/bin/bash

DIRECTORY="/usr/local/bin/ThermalMgmt"
systemctl stop ThermalMgmt.service

if [ -d "$DIRECTORY" ]; then
	echo "Upgrade NanoPiNEO-ThermalMgmt the latest version"
	echo "Uninstall current version"
	rm -rf $DIRECTORY
	rm /lib/systemd/system/ThermalMgmt.service
	systemctl disable ThermalMgmt.service
	systemctl daemon-reload
	
fi

echo "Install ThermalMgmt"
mkdir $DIRECTORY
chmod 710 $DIRECTORY

git -C $DIRECTORY clone --recursive https://gitlab.com/JonW/NanoPiNEO-ThermalMgmt.git .

cp $DIRECTORY/ThermalMgmt.service /lib/systemd/system/ThermalMgmt.service

systemctl daemon-reload
systemctl enable ThermalMgmt.service